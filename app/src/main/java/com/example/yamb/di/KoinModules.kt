package com.example.yamb.di

import com.example.yamb.models.repositories.TicketsRepository
import com.example.yamb.models.repositories.room.SavedTicketsRoomDatabase
import com.example.yamb.ui.diceThrowingFragment.DiceThrowingViewModel
import com.example.yamb.ui.savedTicketsFragment.SavedTicketsViewModel
import com.example.yamb.ui.yambTicketFragment.YambTicketViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { DiceThrowingViewModel(androidApplication()) }
    viewModel { YambTicketViewModel(androidApplication(), get()) }
    viewModel { SavedTicketsViewModel(androidApplication(), get()) }

    single { SavedTicketsRoomDatabase.getInstance(androidContext()).savedTicketsDao() }
    single { TicketsRepository(get()) }
}
