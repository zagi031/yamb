package com.example.yamb.interfaces

interface OnSavedTicketClickListener {
    fun onSavedTicketClick(position: Int)
}