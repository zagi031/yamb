package com.example.yamb

import android.app.Application
import com.example.yamb.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class YambApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@YambApp)
            modules(viewModelModule)
        }
    }
}