package com.example.yamb.models.repositories

import com.example.yamb.models.YambTicket
import com.example.yamb.models.repositories.room.SavedTicketsDao

class TicketsRepository(private val savedTicketsDao: SavedTicketsDao) {
    fun getSavedTickets() = savedTicketsDao.getSavedTickets()
    suspend fun getSavedTicket(startTime: Long) = savedTicketsDao.getSavedTicket(startTime)
    suspend fun saveTicket(yambTicket: YambTicket) = savedTicketsDao.saveTicket(yambTicket)
    suspend fun deleteSavedTicket(yambTicket: YambTicket) =
        savedTicketsDao.deleteSavedTicket(yambTicket.startTime)
}