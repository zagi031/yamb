package com.example.yamb.models.repositories.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.yamb.models.YambTicket

@Database(entities = [YambTicket::class], version = 1)
@TypeConverters(SavedTicketsConverters::class)
abstract class SavedTicketsRoomDatabase : RoomDatabase() {
    abstract fun savedTicketsDao(): SavedTicketsDao

    companion object {
        private const val name = "savedTickets_database"
        private var instance: SavedTicketsRoomDatabase? = null

        fun getInstance(context: Context): SavedTicketsRoomDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(context, SavedTicketsRoomDatabase::class.java, name)
                    .allowMainThreadQueries().fallbackToDestructiveMigration().build()
            }
            return instance as SavedTicketsRoomDatabase
        }
    }
}