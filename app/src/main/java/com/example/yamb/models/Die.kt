package com.example.yamb.models

import com.example.yamb.R

class Die(val id: Int) {
    var value: Int = 0
        private set
    var isHold = false

    init {
        throwDie()
    }

    fun throwDie() {
        this.value = (1..6).random()
    }
}

fun Die.toDieUI() = DieUI(id, value)

class DieUI(val id: Int, val value: Int) {
    fun getDrawableDieIconID(): Int {
        return when (this.value) {
            1 -> R.drawable.ic_die_1
            2 -> R.drawable.ic_die_2
            3 -> R.drawable.ic_die_3
            4 -> R.drawable.ic_die_4
            5 -> R.drawable.ic_die_5
            6 -> R.drawable.ic_die_6
            else -> R.drawable.ic_launcher_background
        }
    }
}