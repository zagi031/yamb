package com.example.yamb.models.repositories.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.yamb.models.YambTicket

@Dao
interface SavedTicketsDao {
    @Query("SELECT * FROM savedTickets")
    fun getSavedTickets(): LiveData<List<YambTicket>>

    @Query("SELECT * FROM savedTickets WHERE startTime = :startTime")
    suspend fun getSavedTicket(startTime: Long): YambTicket

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun saveTicket(yambTicket: YambTicket)

    @Query("DELETE FROM savedTickets WHERE startTime = :startTime")
    suspend fun deleteSavedTicket(startTime: Long)
}