package com.example.yamb.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.yamb.R
import com.example.yamb.ui.yambTicketFragment.YambTicketViewModel

enum class FieldID {
    ColumnLabel,
    LabelUp, LabelDown, LabelUpOrDown, LabelAnnouncement,
    Ones, Twos, Threes, Fours, Fives, Sixes, Max, Min, ThreeOfAKind, Straight, FullHouse, Poker, Yamb,
    UpperSectionSum, MidSectionSum, LowerSectionSum
}

enum class ColumnID { Labels, Up, Down, UpOrDown, Announcement }

class Column(val id: ColumnID, val fields: Array<Field.ValueLabeledField>) {

    companion object {
        const val threeOfAKindNValue = 3
        const val pokerNValue = 4
        const val yambNValue = 5

        const val onesIndex = 0
        const val twosIndex = 1
        const val threesIndex = 2
        const val foursIndex = 3
        const val fivesIndex = 4
        const val sixesIndex = 5
        const val upperSectionSumIndex = 6
        const val maxIndex = 7
        const val minIndex = 8
        const val midSectionSumIndex = 9
        const val threeOfAKindIndex = 10
        const val straightIndex = 11
        const val fullHouseIndex = 12
        const val pokerIndex = 13
        const val yambIndex = 14
        const val lowerSectionSumIndex = 15
    }

    fun setUpperSectionSumField() {
        if (fields[onesIndex].value != null && fields[twosIndex].value != null && fields[threesIndex].value != null && fields[foursIndex].value != null && fields[fivesIndex].value != null && fields[sixesIndex].value != null) {
            fields[upperSectionSumIndex].setValue(
                listOf(
                    fields[onesIndex],
                    fields[twosIndex],
                    fields[threesIndex],
                    fields[foursIndex],
                    fields[fivesIndex],
                    fields[sixesIndex]
                ),
                UpperSectionSumFieldCalculator()
            )
        }
    }

    fun setMidSectionSumField() {
        if (fields[onesIndex].value != null && fields[maxIndex].value != null && fields[minIndex].value != null) {
            fields[midSectionSumIndex].setValue(
                listOf(fields[onesIndex], fields[maxIndex], fields[minIndex]),
                MidSectionSumFieldCalculator()
            )
        }
    }

    fun setLowerSectionSumField() {
        if (fields[threeOfAKindIndex].value != null && fields[straightIndex].value != null && fields[fullHouseIndex].value != null && fields[pokerIndex].value != null && fields[yambIndex].value != null) {
            fields[lowerSectionSumIndex].setValue(
                listOf(
                    fields[threeOfAKindIndex],
                    fields[straightIndex],
                    fields[fullHouseIndex],
                    fields[pokerIndex],
                    fields[yambIndex]
                ),
                LowerSectionSumFieldCalculator()
            )
        }
    }

    fun setFieldValue(position: Int, dice: List<DieUI>, numberOfThrows: Int) {
        val field = fields[position]
        when (position) {
            in (onesIndex..sixesIndex) -> field.setValue(dice, UpperSectionCalculator(position + 1))
            maxIndex -> field.setValue(dice, MidSectionCalculator(true))
            minIndex -> field.setValue(dice, MidSectionCalculator(false))
            threeOfAKindIndex -> field.setValue(dice, NSameDieValuesCalculator(threeOfAKindNValue))
            straightIndex -> field.setValue(dice, StraightCalculator(), numberOfThrows)
            fullHouseIndex -> field.setValue(dice, FullHouseCalculator())
            pokerIndex -> field.setValue(dice, NSameDieValuesCalculator(pokerNValue))
            yambIndex -> field.setValue(dice, NSameDieValuesCalculator(yambNValue))
        }
    }
}

@Suppress("UNCHECKED_CAST")
fun Column.toColumnUI() = ColumnUI(id, fields as Array<Field>)

class ColumnUI(val id: ColumnID, val fields: Array<Field>) {
    companion object {
        const val upperSectionSumIndex = 7
        const val midSectionSumIndex = 10
        const val lowerSectionSumIndex = 16
    }
}

@Entity(tableName = "savedTickets")
class YambTicket {
    @PrimaryKey
    var startTime = System.currentTimeMillis()
    var endTime: Long? = null
    var columns = instantiateTicket()

    @Ignore
    var announcedFieldRowPosition: Int? = null
    private fun instantiateTicket(): Array<Column> {
        val columnDown = Column(ColumnID.Down, instantiateColumn())
        val columnUp = Column(ColumnID.Up, instantiateColumn())
        val columnUpOrDown = Column(ColumnID.UpOrDown, instantiateColumn())
        val columnAnnouncement = Column(ColumnID.Announcement, instantiateColumn())
        return arrayOf(columnDown, columnUp, columnUpOrDown, columnAnnouncement)
    }

    fun setEndingTime() {
        this.endTime = System.currentTimeMillis()
    }

    private fun instantiateColumn(): Array<Field.ValueLabeledField> {
        return arrayOf(
            Field.ValueLabeledField(FieldID.Ones),
            Field.ValueLabeledField(FieldID.Twos),
            Field.ValueLabeledField(FieldID.Threes),
            Field.ValueLabeledField(FieldID.Fours),
            Field.ValueLabeledField(FieldID.Fives),
            Field.ValueLabeledField(FieldID.Sixes),
            Field.ValueLabeledField(FieldID.UpperSectionSum),
            Field.ValueLabeledField(FieldID.Max),
            Field.ValueLabeledField(FieldID.Min),
            Field.ValueLabeledField(FieldID.MidSectionSum),
            Field.ValueLabeledField(FieldID.ThreeOfAKind),
            Field.ValueLabeledField(FieldID.Straight),
            Field.ValueLabeledField(FieldID.FullHouse),
            Field.ValueLabeledField(FieldID.Poker),
            Field.ValueLabeledField(FieldID.Yamb),
            Field.ValueLabeledField(FieldID.LowerSectionSum),
        )
    }

    fun isColumnDownRuleSatisfied(rowPosition: Int): Boolean {
        val columnPosition = 0
        return if (rowPosition == 0) true else columns[columnPosition].fields[rowPosition - 1].value != null
    }

    fun isColumnUpRuleSatisfied(rowPosition: Int): Boolean {
        val columnPosition = 1
        return if (rowPosition == Column.yambIndex) true
        else if (rowPosition == Column.minIndex || rowPosition == Column.sixesIndex)
            columns[columnPosition].fields[rowPosition + 2].value != null
        else columns[columnPosition].fields[rowPosition + 1].value != null
    }

    fun canAnnounceField(columnPosition: Int, numberOfThrows: Int) =
        columnPosition == YambTicketViewModel.announcementColumnIndex && numberOfThrows == 1 && announcedFieldRowPosition == null

    fun announceField(rowPosition: Int) {
        this.announcedFieldRowPosition = rowPosition
    }

    fun isFieldAnnounced(fieldCoordinates: FieldCoordinates) =
        fieldCoordinates.columnPosition == YambTicketViewModel.announcementColumnIndex && announcedFieldRowPosition == fieldCoordinates.rowPosition

    fun resetAnnouncedField() {
        announcedFieldRowPosition = null
    }

    fun getTotalSum(): Int {
        var sum = 0
        columns.forEach { column ->
            sum += column.fields[Column.upperSectionSumIndex].value ?: 0
            sum += column.fields[Column.midSectionSumIndex].value ?: 0
            sum += column.fields[Column.lowerSectionSumIndex].value ?: 0
        }
        return sum
    }
}

fun YambTicket.toYambTicketUI() =
    YambTicketUI(columns)

class YambTicketUI(
    private val modelColumns: Array<Column>
) {
    val columns = instantiateTicket()

    private fun instantiateTicket(): Array<ColumnUI> {
        val newColumns = mutableListOf<ColumnUI>().apply {
            add(instantiateColumn(ColumnID.Labels))
            add(instantiateColumn(ColumnID.Down))
            add(instantiateColumn(ColumnID.Up))
            add(instantiateColumn(ColumnID.UpOrDown))
            add(instantiateColumn(ColumnID.Announcement))
        }
        return newColumns.toTypedArray()
    }

    @Suppress("UNCHECKED_CAST")
    private fun instantiateColumn(columnID: ColumnID): ColumnUI {
        return when (columnID) {
            ColumnID.Labels -> ColumnUI(ColumnID.Labels, instantiateFirstColumnFields())
            ColumnID.Down -> {
                ColumnUI(
                    ColumnID.Down,
                    getModifiedColumnFields(
                        Field.IconLabeledField(FieldID.LabelDown, R.drawable.ic_down),
                        modelColumns.first { it.id == ColumnID.Down }.fields as Array<Field>
                    )
                )
            }
            ColumnID.Up -> {
                ColumnUI(
                    ColumnID.Up,
                    getModifiedColumnFields(
                        Field.IconLabeledField(FieldID.LabelUp, R.drawable.ic_up),
                        modelColumns.first { it.id == ColumnID.Up }.fields as Array<Field>
                    )
                )
            }
            ColumnID.UpOrDown -> {
                ColumnUI(
                    ColumnID.UpOrDown,
                    getModifiedColumnFields(
                        Field.IconLabeledField(FieldID.LabelUpOrDown, R.drawable.ic_up_or_down),
                        modelColumns.first { it.id == ColumnID.UpOrDown }.fields as Array<Field>
                    )
                )
            }
            else -> {
                ColumnUI(
                    ColumnID.Announcement,
                    getModifiedColumnFields(
                        Field.TextLabeledField(
                            FieldID.LabelAnnouncement,
                            R.string.announcementColumnLabel
                        ),
                        modelColumns.first { it.id == ColumnID.Announcement }.fields as Array<Field>
                    )
                )
            }  // ColumnID.Announcement
        }
    }

    private fun instantiateFirstColumnFields(): Array<Field> {
        return arrayOf(
            Field.TextLabeledField(FieldID.ColumnLabel, null),
            Field.IconLabeledField(FieldID.Ones, R.drawable.ic_die_1),
            Field.IconLabeledField(FieldID.Twos, R.drawable.ic_die_2),
            Field.IconLabeledField(FieldID.Threes, R.drawable.ic_die_3),
            Field.IconLabeledField(FieldID.Fours, R.drawable.ic_die_4),
            Field.IconLabeledField(FieldID.Fives, R.drawable.ic_die_5),
            Field.IconLabeledField(FieldID.Sixes, R.drawable.ic_die_6),
            Field.TextLabeledField(FieldID.UpperSectionSum, R.string.sign_sigma),
            Field.TextLabeledField(FieldID.Max, R.string.label_max),
            Field.TextLabeledField(FieldID.Min, R.string.label_min),
            Field.TextLabeledField(FieldID.MidSectionSum, R.string.sign_sigma),
            Field.TextLabeledField(FieldID.ThreeOfAKind, R.string.label_three_of_a_kind),
            Field.TextLabeledField(FieldID.Straight, R.string.label_straight),
            Field.TextLabeledField(FieldID.FullHouse, R.string.label_full_house),
            Field.TextLabeledField(FieldID.Poker, R.string.label_poker),
            Field.TextLabeledField(FieldID.Yamb, R.string.label_yamb),
            Field.TextLabeledField(FieldID.LowerSectionSum, R.string.sign_sigma)
        )
    }

    private fun getModifiedColumnFields(field: Field, fields: Array<Field>): Array<Field> {
        val list: MutableList<Field> = fields.toMutableList()
        list.add(0, field)
        return list.toTypedArray()
    }
}

sealed class Field(val id: FieldID) {
    class ValueLabeledField(id: FieldID) : Field(id) {
        var value: Int? = null
            private set

        fun setValue(dice: List<DieUI>, calculator: FieldValueCalculator) {
            this.value = calculator.calculateValue(dice)
        }

        fun setValue(
            dice: List<DieUI>,
            calculator: FieldValueCalculatorDependingOnNumberOfThrows,
            numberOfThrows: Int
        ) {
            this.value = calculator.calculateValue(dice, numberOfThrows)
        }

        fun setValue(
            fields: List<ValueLabeledField>,
            calculator: FieldValueCalculatorDependingOnOtherFields
        ) {
            this.value = calculator.calculateValue(fields)
        }

        fun setMinimumValue() {
            this.value = 0
        }
    }

    class TextLabeledField(id: FieldID, val stringResID: Int?) : Field(id)
    class IconLabeledField(id: FieldID, val drawableResID: Int) : Field(id)
}

