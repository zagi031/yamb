package com.example.yamb.models

import com.example.yamb.ui.yambTicketFragment.YambFieldRecyclerViewAdapter
import com.example.yamb.ui.yambTicketFragment.YambTicketViewModel.Companion.NUM_OF_COLUMNS

open class FieldCoordinates(val position: Int, val numberOfColumns: Int) {
    val rowPosition = getRowPosition(position, numberOfColumns)
    val columnPosition = getColumnPosition(position, numberOfColumns)

    private fun getRowPosition(position: Int, numberOfColumns: Int) = position / numberOfColumns

    private fun getColumnPosition(position: Int, numberOfColumns: Int) = position % numberOfColumns
}

class UIFieldCoordinates(position: Int, numberOfColumns: Int) :
    FieldCoordinates(position, numberOfColumns) {
    fun getModelPositionFromUIPosition() = FieldCoordinates(
        position - YambFieldRecyclerViewAdapter.NUM_OF_UICOLUMNS - (position / YambFieldRecyclerViewAdapter.NUM_OF_UICOLUMNS),
        NUM_OF_COLUMNS
    )
}