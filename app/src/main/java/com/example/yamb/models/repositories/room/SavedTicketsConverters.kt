package com.example.yamb.models.repositories.room

import androidx.room.TypeConverter
import com.example.yamb.models.Column
import com.google.gson.Gson

class SavedTicketsConverters {
    @TypeConverter
    fun columnsToString(columns: Array<Column>): String {
        return Gson().toJson(columns)
    }

    @TypeConverter
    fun stringToColumns(json: String): Array<Column> {
        return Gson().fromJson(json, Array<Column>::class.java)
    }
}