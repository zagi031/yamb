package com.example.yamb.models

interface FieldValueCalculator {
    fun calculateValue(dice: List<DieUI>): Int
}

interface FieldValueCalculatorDependingOnNumberOfThrows {
    fun calculateValue(dice: List<DieUI>, numberOfThrows: Int): Int
}

interface FieldValueCalculatorDependingOnOtherFields {
    fun calculateValue(fields: List<Field.ValueLabeledField>): Int
}

class UpperSectionCalculator(private val valueToCount: Int) : FieldValueCalculator {
    override fun calculateValue(dice: List<DieUI>): Int {
        val numberOfDiceToCount = 5
        return dice.map { it.value }.filter { it == valueToCount }.take(numberOfDiceToCount).count() * valueToCount
    }
}

class MidSectionCalculator(private val getMaxValue: Boolean) : FieldValueCalculator {
    override fun calculateValue(dice: List<DieUI>): Int {
        val numberOfDiceToCount = 5
        return if (getMaxValue) {
            dice.map { it.value }.sortedDescending().take(numberOfDiceToCount).sum()
        } else {
            dice.map { it.value }.sorted().take(numberOfDiceToCount).sum()
        }
    }
}

class NSameDieValuesCalculator(private val n: Int) : FieldValueCalculator {
    override fun calculateValue(dice: List<DieUI>): Int {
        val bonus = n * 10
        val filteredValuesByDice =
            dice.groupingBy { die -> die.value }.eachCount().filter { it.value >= n }
        return if (filteredValuesByDice.isNotEmpty()) {
            val dieValue = filteredValuesByDice.maxOf { it.key }
            return (n * dieValue) + bonus
        } else 0
    }
}

class FullHouseCalculator : FieldValueCalculator {
    override fun calculateValue(dice: List<DieUI>): Int {
        val bonus = 30
        val valuesByDice = dice.groupingBy { die -> die.value }.eachCount()
        val twoOfAKindDieValue = valuesByDice.filterValues { it == 2 }.keys
        val threeOfAKindDieValue = valuesByDice.filterValues { it == 3 }.keys.sorted()
        val fourOfAKindDieValue = valuesByDice.filterValues { it == 4 }.keys.sorted()
        return if (twoOfAKindDieValue.isNotEmpty() && threeOfAKindDieValue.isNotEmpty())
            (twoOfAKindDieValue.first() * 2) + (threeOfAKindDieValue.first() * 3) + bonus
        else if (threeOfAKindDieValue.size == 2)
            (threeOfAKindDieValue[0] * 2) + (threeOfAKindDieValue[1] * 3) + bonus
        else if (fourOfAKindDieValue.isNotEmpty())
            (fourOfAKindDieValue.first() * 3) + (twoOfAKindDieValue.first() * 2) + bonus
        else 0
    }
}

class StraightCalculator : FieldValueCalculatorDependingOnNumberOfThrows {
    override fun calculateValue(dice: List<DieUI>, numberOfThrows: Int): Int {
        val firstThrowingStraightPoints = 66
        val secondThrowingStraightPoints = 56
        val thirdThrowingStraightPoints = 46
        val diceValues = dice.map { it.value }.toSet()
            .sorted() // first we need to remove duplicates with toSet() and then sorted() converts to list
        return if (diceValues.size < 5) 0
        else when (numberOfThrows) {
            1 -> firstThrowingStraightPoints
            2 -> secondThrowingStraightPoints
            else -> thirdThrowingStraightPoints
        }
    }
}

class UpperSectionSumFieldCalculator : FieldValueCalculatorDependingOnOtherFields {
    override fun calculateValue(fields: List<Field.ValueLabeledField>): Int {
        val bonus = 30
        var sum = 0
        fields.forEach { field ->
            sum += field.value ?: 0
        }
        return if (sum >= 60) sum + bonus else sum
    }

}

class MidSectionSumFieldCalculator : FieldValueCalculatorDependingOnOtherFields {
    override fun calculateValue(fields: List<Field.ValueLabeledField>): Int {
        val numberOfOnes = fields.first { it.id == FieldID.Ones }.value!!
        val max = fields.first { it.id == FieldID.Max }.value!!
        val min = fields.first { it.id == FieldID.Min }.value!!
        return if (max > min) (max - min) * numberOfOnes else 0
    }
}

class LowerSectionSumFieldCalculator : FieldValueCalculatorDependingOnOtherFields {
    override fun calculateValue(fields: List<Field.ValueLabeledField>): Int {
        var result = 0
        fields.forEach { field ->
            result += field.value ?: 0
        }
        return result
    }
}