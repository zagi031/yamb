package com.example.yamb.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    @SuppressLint("SimpleDateFormat")
    private val format = SimpleDateFormat("dd.MM.yyyy HH:mm")
    fun convertLongToString(time: Long): String {
        return format.format(Date(time))
    }
}