package com.example.yamb.ui.savedTicketsFragment.adapters

import android.annotation.SuppressLint
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.yamb.databinding.RvItemEmptyrvmessageBinding
import com.example.yamb.databinding.RvItemTicketBinding
import com.example.yamb.interfaces.OnSavedTicketClickListener
import com.example.yamb.models.YambTicket
import com.example.yamb.utils.DateHelper

class SavedTicketsRecyclerViewAdapter(private val emptyRecyclerViewMessage: String, private val listener: OnSavedTicketClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val NON_EMPTY_TYPE = 100
        private const val EMPTY_TYPE = 101
    }

    private val savedTickets = mutableListOf<YambTicket>()

    fun setTickets(tickets: List<YambTicket>) {
        this.savedTickets.clear()
        this.savedTickets.addAll(tickets)
        this.notifyDataSetChanged()
    }

    fun getTicket(position: Int): YambTicket? {
        return if (this.savedTickets.isNotEmpty()) {
            this.savedTickets[position]
        } else null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NON_EMPTY_TYPE) {
            SavedTicketViewHolder(
                RvItemTicketBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            NoneSavedTicketsViewHolder(
                RvItemEmptyrvmessageBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == NON_EMPTY_TYPE) {
            (holder as SavedTicketViewHolder).bind(this.savedTickets[position], listener)
        } else (holder as NoneSavedTicketsViewHolder).bind(emptyRecyclerViewMessage)
    }

    override fun getItemCount() =
        if (savedTickets.isNotEmpty()) savedTickets.size else 1 // showing one message item that list is empty

    override fun getItemViewType(position: Int) =
        if (this.savedTickets.isNotEmpty()) NON_EMPTY_TYPE else EMPTY_TYPE
}

class SavedTicketViewHolder(private val binding: RvItemTicketBinding) :
    RecyclerView.ViewHolder(binding.root) {
    @SuppressLint("SetTextI18n")
    fun bind(yambTicket: YambTicket, listener: OnSavedTicketClickListener) {
        binding.apply {
            tvStartTime.text =
                "Game started: ${DateHelper.convertLongToString(yambTicket.startTime)}"
            tvEndTime.text =
                "Game ended: ${yambTicket.endTime?.let { DateHelper.convertLongToString(it) }}"
            tvScore.text = "Score: ${yambTicket.getTotalSum()}"
        }
        binding.root.setOnClickListener {
            listener.onSavedTicketClick(adapterPosition)
        }
    }
}

class NoneSavedTicketsViewHolder(private val binding: RvItemEmptyrvmessageBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(message: String) {
        binding.tvMessage.text = message
    }
}

class SavedTicketItemDecorator(
    private val verticalSpacingHeight: Int,
    private val horizontalSpacingWidth: Int
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.offset(verticalSpacingHeight, horizontalSpacingWidth)
    }
}
