package com.example.yamb.ui.newGameFragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.example.yamb.R
import com.example.yamb.databinding.FragmentNewGameBinding
import com.google.android.material.tabs.TabLayoutMediator

class NewGameFragment : Fragment() {
    private lateinit var binding: FragmentNewGameBinding
    private lateinit var viewpagerAdapter: ViewPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewPager()
        setTabLayout()
    }

    private fun setViewPager() {
        this.viewpagerAdapter = ViewPagerAdapter(childFragmentManager, lifecycle)
        binding.viewpager.adapter = this.viewpagerAdapter
    }

    private fun setTabLayout() {
        TabLayoutMediator(binding.tabLayout, binding.viewpager) { tab, position ->
            tab.text = when (position) {
                0 -> resources.getString(R.string.label_yambTicketFragment)
                else -> resources.getString(R.string.label_diceThrowingFragment)
            }
        }.attach()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                AlertDialog.Builder(activity).setTitle(resources.getString(R.string.label_warning))
                    .setMessage(resources.getString(R.string.message_alertDialogOnExitingGame))
                    .setPositiveButton(resources.getString(R.string.label_yes)) { _, _ ->
                        isEnabled = false
                        activity?.onBackPressed()
                    }.setNegativeButton(resources.getString(R.string.label_no)) { dialog, _ ->
                        dialog.dismiss()
                    }.create().show()
            }
        })
    }

}