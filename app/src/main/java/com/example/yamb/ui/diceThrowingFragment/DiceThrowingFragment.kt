package com.example.yamb.ui.diceThrowingFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.yamb.databinding.FragmentDiceThrowingBinding
import com.example.yamb.ui.mainActivity.ThrownDiceSharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class DiceThrowingFragment : Fragment() {
    private lateinit var binding: FragmentDiceThrowingBinding
    private val viewModel by viewModel<DiceThrowingViewModel>()
    private val sharedViewModel by activityViewModels<ThrownDiceSharedViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDiceThrowingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setImageButtonsVisibility(false)
        val imageButtons = getImageButtonsDiceReferences()
        indexImageButtons(imageButtons)
        imageButtons.forEach { imageButton ->
            imageButton.setOnClickListener {
                viewModel.handleDieClickEvent(imageButton.tag as Int)
            }
        }

        viewModel.dice.observe(viewLifecycleOwner, { dice ->
            sharedViewModel.thrownDice = dice   // update sharedViewModel
            dice.forEach { die ->
                val imageButton = imageButtons.first { it.tag == die.id }
                imageButton.setImageResource(die.getDrawableDieIconID())
                imageButton.backgroundTintList = AppCompatResources.getColorStateList(
                    requireActivity().baseContext,
                    viewModel.getColorForImageButtonBorder(imageButton.tag as Int)
                )
            }
            if (binding.glDice.visibility != View.VISIBLE) setImageButtonsVisibility(true)
        })
        viewModel.toast.observe(viewLifecycleOwner, { message ->
            showToast(message)
        })
        viewModel.numberOfThrows.observe(viewLifecycleOwner, {
            if (it == 0) setImageButtonsVisibility(false) else animateDice(imageButtons)
            sharedViewModel.numberOfThrows = it // update sharedViewModel
        })
        sharedViewModel.isChangedField.observe(viewLifecycleOwner, {
            if (it) {
                viewModel.resetNumberOfThrows()
                setImageButtonsVisibility(false)
            }
        })

        binding.btnThrow.setOnClickListener {
            viewModel.throwDice()
        }
    }

    private fun animateDice(imageButtons: List<ImageButton>) {
        imageButtons.forEach { imageButton ->
            if (!viewModel.isDieHold(imageButton.tag as Int)) {
                YoYo.with(Techniques.Wave).duration(300).playOn(imageButton)
            }
        }
    }

    private fun getImageButtonsDiceReferences() = listOf(
        binding.imgbtn1,
        binding.imgbtn2,
        binding.imgbtn3,
        binding.imgbtn4,
        binding.imgbtn5,
        binding.imgbtn6
    )

    private fun indexImageButtons(imageButtons: List<ImageButton>) {
        val diceIDs = viewModel.getDiceIDs()
        imageButtons.forEachIndexed { index, imageButton ->
            imageButton.tag = diceIDs[index]
        }
    }

    private fun setImageButtonsVisibility(show: Boolean) {
        val visibility = if (show) View.VISIBLE else View.INVISIBLE
        binding.glDice.visibility = visibility
    }

    private fun showToast(message: String) =
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
}