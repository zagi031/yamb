package com.example.yamb.ui.yambTicketFragment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.yamb.models.*
import com.example.yamb.models.Column.Companion.lowerSectionSumIndex
import com.example.yamb.models.Column.Companion.maxIndex
import com.example.yamb.models.Column.Companion.midSectionSumIndex
import com.example.yamb.models.Column.Companion.minIndex
import com.example.yamb.models.Column.Companion.onesIndex
import com.example.yamb.models.Column.Companion.sixesIndex
import com.example.yamb.models.Column.Companion.threeOfAKindIndex
import com.example.yamb.models.Column.Companion.upperSectionSumIndex
import com.example.yamb.models.Column.Companion.yambIndex
import com.example.yamb.models.repositories.TicketsRepository
import com.example.yamb.ui.SingleLiveEvent
import com.example.yamb.ui.yambTicketFragment.YambFieldRecyclerViewAdapter.Companion.NUM_OF_UICOLUMNS
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class YambTicketViewModel(application: Application, private val repository: TicketsRepository) :
    AndroidViewModel(application) {
    companion object {
        const val NUM_OF_COLUMNS = 4
        const val downColumnIndex = 0
        const val upColumnIndex = 1
        const val upOrDownColumnIndex = 2
        const val announcementColumnIndex = 3
        private const val defaultStartTime = -1L
    }

    private var ticket = YambTicket()
    private val _toast = SingleLiveEvent<String>()
    val toast: LiveData<String> = _toast
    private val _changedFields = SingleLiveEvent<List<Pair<Field.ValueLabeledField, Int>>>()
    val changedFields: LiveData<List<Pair<Field.ValueLabeledField, Int>>> =
        _changedFields  // updates clicked field and sum fields in that column
    private val _isGameOver = SingleLiveEvent<Boolean>()
    val isGameOver: LiveData<Boolean> = _isGameOver

    fun getYambTicket(startTime: Long): YambTicketUI {
        if (startTime != defaultStartTime) {
            val result = viewModelScope.async(Dispatchers.IO) {
                repository.getSavedTicket(startTime)
            }
            runBlocking { ticket = result.await() }
        }
        return ticket.toYambTicketUI()
    }

    fun handleOnFieldClick(position: Int, dice: List<DieUI>?, numberOfThrows: Int?) {
        val UIcoordinates = UIFieldCoordinates(position, NUM_OF_UICOLUMNS)
        val fieldModelCoordinates = UIcoordinates.getModelPositionFromUIPosition()
        if (numberOfThrows != null && numberOfThrows > 0 && dice != null) {
            if (ticket.canAnnounceField(fieldModelCoordinates.columnPosition, numberOfThrows)) {
                ticket.announceField(fieldModelCoordinates.rowPosition)
                _toast.postValue("Field ${ticket.columns[fieldModelCoordinates.columnPosition].fields[fieldModelCoordinates.rowPosition].id.name} announced")
                return
            }
            if (canSetFieldByRules(fieldModelCoordinates)) {
                ticket.columns[fieldModelCoordinates.columnPosition].setFieldValue(
                    fieldModelCoordinates.rowPosition,
                    dice,
                    numberOfThrows
                )
                if (ticket.isFieldAnnounced(fieldModelCoordinates)) ticket.resetAnnouncedField()
                setSumField(fieldModelCoordinates)
                notifyObserversOnChangedFields(fieldModelCoordinates, UIcoordinates)
                checkIfGameOverRoutine()
            } else if (fieldModelCoordinates.columnPosition == announcementColumnIndex && numberOfThrows > 1 && ticket.announcedFieldRowPosition == null) {
                ticket.columns[fieldModelCoordinates.columnPosition].fields[fieldModelCoordinates.rowPosition].setMinimumValue()   // in case of last positions we forgot to announce field after first dice throwing
                setSumField(fieldModelCoordinates)
                notifyObserversOnChangedFields(fieldModelCoordinates, UIcoordinates)
                checkIfGameOverRoutine()
            } else _toast.postValue("Cannot play against the rules")
        } else _toast.postValue("Please throw the dice")
    }

    private fun setSumField(fieldCoordinates: FieldCoordinates) {
        when (fieldCoordinates.rowPosition) {
            in (onesIndex..sixesIndex) -> {
                ticket.columns[fieldCoordinates.columnPosition].setUpperSectionSumField()
                ticket.columns[fieldCoordinates.columnPosition].setMidSectionSumField()
            }
            in (maxIndex..minIndex) -> ticket.columns[fieldCoordinates.columnPosition].setMidSectionSumField()
            in (threeOfAKindIndex..yambIndex) -> ticket.columns[fieldCoordinates.columnPosition].setLowerSectionSumField()
        }
    }

    private fun notifyObserversOnChangedFields(
        modelCoordinates: FieldCoordinates,
        UIcoordinates: FieldCoordinates
    ) {
        val updatedFields = mutableListOf<Pair<Field.ValueLabeledField, Int>>()
        updatedFields.add(
            Pair(
                ticket.columns[modelCoordinates.columnPosition].fields[modelCoordinates.rowPosition],
                (UIcoordinates.numberOfColumns * UIcoordinates.rowPosition) + UIcoordinates.columnPosition
            )
        )
        when (modelCoordinates.rowPosition) {
            in (onesIndex..sixesIndex) -> {
                updatedFields.add(
                    Pair(
                        ticket.columns[modelCoordinates.columnPosition].fields[upperSectionSumIndex],
                        (UIcoordinates.numberOfColumns * ColumnUI.upperSectionSumIndex) + UIcoordinates.columnPosition
                    )
                )   // upper sum field
                updatedFields.add(
                    Pair(
                        ticket.columns[modelCoordinates.columnPosition].fields[midSectionSumIndex],
                        (UIcoordinates.numberOfColumns * ColumnUI.midSectionSumIndex) + UIcoordinates.columnPosition
                    )
                )   // mid sum field
            }
            in (maxIndex..minIndex) -> updatedFields.add(
                Pair(
                    ticket.columns[modelCoordinates.columnPosition].fields[midSectionSumIndex],
                    (UIcoordinates.numberOfColumns * ColumnUI.midSectionSumIndex) + UIcoordinates.columnPosition
                )
            )   // mid sum field
            in (threeOfAKindIndex..yambIndex) -> updatedFields.add(
                Pair(
                    ticket.columns[modelCoordinates.columnPosition].fields[lowerSectionSumIndex],
                    (UIcoordinates.numberOfColumns * ColumnUI.lowerSectionSumIndex) + UIcoordinates.columnPosition
                )
            ) // lower sum field
        }
        _changedFields.postValue(updatedFields)
    }

    private fun canSetFieldByRules(fieldCoordinates: FieldCoordinates): Boolean {
        return when (fieldCoordinates.columnPosition) {
            downColumnIndex -> ticket.isColumnDownRuleSatisfied(fieldCoordinates.rowPosition) && ticket.announcedFieldRowPosition == null
            upColumnIndex -> ticket.isColumnUpRuleSatisfied(fieldCoordinates.rowPosition) && ticket.announcedFieldRowPosition == null
            upOrDownColumnIndex -> ticket.announcedFieldRowPosition == null  // UpOrDown column has no conditions
            else -> ticket.isFieldAnnounced(fieldCoordinates)
        }
    }

    private fun checkIfGameOverRoutine() {
        val isGameOver = isGameOver()
        if (isGameOver) {
            ticket.setEndingTime()
            _isGameOver.postValue(isGameOver)
            viewModelScope.launch(Dispatchers.IO) {
                repository.saveTicket(ticket)
            }
        }
    }

    private fun isGameOver(): Boolean {
        ticket.columns.forEach { column ->
            if (column.fields[upperSectionSumIndex].value == null
                || column.fields[midSectionSumIndex].value == null
                || column.fields[lowerSectionSumIndex].value == null
            ) return false
        }
        return true
    }

    fun getTotalScore() = ticket.getTotalSum()
}