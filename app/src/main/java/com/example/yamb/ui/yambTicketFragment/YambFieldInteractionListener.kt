package com.example.yamb.ui.yambTicketFragment

interface YambFieldInteractionListener {
    fun onFieldClick(position: Int)
}