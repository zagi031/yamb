package com.example.yamb.ui.yambTicketFragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.yamb.R
import com.example.yamb.databinding.FragmentYambTicketBinding
import com.example.yamb.ui.mainActivity.ThrownDiceSharedViewModel
import com.example.yamb.ui.yambTicketFragment.YambFieldRecyclerViewAdapter.Companion.NUM_OF_UICOLUMNS
import org.koin.androidx.viewmodel.ext.android.viewModel

class YambTicketFragment : Fragment(), YambFieldInteractionListener {
    private lateinit var binding: FragmentYambTicketBinding
    private val viewModel by viewModel<YambTicketViewModel>()
    private val fieldAdapter = YambFieldRecyclerViewAdapter(this@YambTicketFragment)
    private val sharedViewModel by activityViewModels<ThrownDiceSharedViewModel>()
    private val args: YambTicketFragmentArgs by navArgs()

    companion object {
        private const val defaultSavedTicketStartTime = -1L

        @JvmStatic
        fun newInstance(readOnly: Boolean) =
            YambTicketFragment().apply {
                if (readOnly) {
                    arguments = Bundle().apply {
                        putLong("savedTicketId", defaultSavedTicketStartTime)
                    }
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentYambTicketBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        fieldAdapter.setYambTicket(viewModel.getYambTicket(args.savedTicketId))
        viewModel.changedFields.observe(viewLifecycleOwner, {
            it.forEach { pair ->
                fieldAdapter.updateField(pair.first, pair.second)
                sharedViewModel.notifyThatFieldIsChanged()
            }
        })
        viewModel.toast.observe(viewLifecycleOwner, { message ->
            showToast(message)
        })
        viewModel.isGameOver.observe(viewLifecycleOwner, {
            if (it) showGameOverDialog()
        })
    }

    private fun setupRecyclerView() {
        binding.apply {
            rvFields.apply {
                adapter = this@YambTicketFragment.fieldAdapter
                val gridLayoutManager =
                    GridLayoutManager(
                        this@YambTicketFragment.parentFragment?.context,
                        NUM_OF_UICOLUMNS
                    )
                layoutManager = gridLayoutManager
            }
        }
    }

    private fun showGameOverDialog() {
        AlertDialog.Builder(activity)
            .setTitle(resources.getString(R.string.title_alertDialogGameOver))
            .setMessage(resources.getString(R.string.message_alertDialogGameOver) + " ${viewModel.getTotalScore()} points")
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }.create().show()
    }

    override fun onFieldClick(position: Int) {
        viewModel.handleOnFieldClick(
            position,
            sharedViewModel.thrownDice,
            sharedViewModel.numberOfThrows
        )
    }

    private fun showToast(message: String) =
        Toast.makeText(activity?.baseContext, message, Toast.LENGTH_SHORT).show()

    override fun onDestroyView() {
        super.onDestroyView()
        sharedViewModel.resetStates()
    }
}