package com.example.yamb.ui.yambTicketFragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.yamb.R
import com.example.yamb.databinding.RvItemFieldBinding
import com.example.yamb.databinding.RvItemIconlabeledFieldBinding
import com.example.yamb.databinding.RvItemTextlabeledFieldBinding
import com.example.yamb.models.*

class YambFieldRecyclerViewAdapter(private val listener: YambFieldInteractionListener?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val NUM_OF_UICOLUMNS = 5

        private const val VALUELABELED_TYPE = 200
        private const val ICONLABELED_TYPE = 201
        private const val TEXTLABELED_TYPE = 202
    }

    private lateinit var yambTicket: YambTicketUI

    fun setYambTicket(yambTicket: YambTicketUI) {
        this.yambTicket = yambTicket
        notifyDataSetChanged()
    }

    fun updateField(field: Field.ValueLabeledField, position: Int) {
        val fieldCoordinates = UIFieldCoordinates(position, NUM_OF_UICOLUMNS)
        if (field.value != null) {
            yambTicket.columns[fieldCoordinates.columnPosition]
                .fields[fieldCoordinates.rowPosition] = field
            notifyItemChanged(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TEXTLABELED_TYPE -> TextLabeledFieldViewHolder(
                RvItemTextlabeledFieldBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
            ICONLABELED_TYPE -> IconLabeledFieldViewHolder(
                RvItemIconlabeledFieldBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            else -> FieldViewHolder(
                RvItemFieldBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        val fieldCoordinates = UIFieldCoordinates(position, NUM_OF_UICOLUMNS)
        return when (yambTicket.columns[fieldCoordinates.columnPosition].fields[fieldCoordinates.rowPosition]) {
            is Field.IconLabeledField -> ICONLABELED_TYPE
            is Field.TextLabeledField -> TEXTLABELED_TYPE
            is Field.ValueLabeledField -> VALUELABELED_TYPE
        }
    }

    override fun getItemCount() =
        yambTicket.columns.size * yambTicket.columns.first().fields.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val fieldCoordinates = UIFieldCoordinates(position, NUM_OF_UICOLUMNS)
        val field =
            yambTicket.columns[fieldCoordinates.columnPosition].fields[fieldCoordinates.rowPosition]
        when (getItemViewType(position)) {
            VALUELABELED_TYPE -> (holder as FieldViewHolder).bind(
                field as Field.ValueLabeledField,
                listener
            )
            ICONLABELED_TYPE -> (holder as IconLabeledFieldViewHolder).bind(field as Field.IconLabeledField)
            TEXTLABELED_TYPE -> (holder as TextLabeledFieldViewHolder).bind(field as Field.TextLabeledField)
        }
    }
}

class FieldViewHolder(private val binding: RvItemFieldBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(field: Field.ValueLabeledField, listener: YambFieldInteractionListener?) {
        binding.tvFieldValue.text = if (field.value == null) "-" else field.value.toString()
        if (field.id == FieldID.UpperSectionSum || field.id == FieldID.MidSectionSum || field.id == FieldID.LowerSectionSum) {
            binding.root.isEnabled = false
            binding.root.setCardBackgroundColor(this.itemView.context.resources.getColor(R.color.purple_500))
            binding.tvFieldValue.setTextColor(this.itemView.context.resources.getColor(R.color.white))
        } else {
            binding.root.isEnabled = true
            binding.root.setCardBackgroundColor(this.itemView.context.resources.getColor(R.color.white))
            binding.tvFieldValue.setTextColor(this.itemView.context.resources.getColor(R.color.black))
            if (field.value == null) {
                binding.root.setOnClickListener { listener?.onFieldClick(adapterPosition) }
            } else {    // prevent to change field value if it had already its value
                binding.root.isEnabled = false
            }
        }
    }
}

class IconLabeledFieldViewHolder(private val binding: RvItemIconlabeledFieldBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(field: Field.IconLabeledField) {
        val drawable = itemView.context.resources.getDrawable(field.drawableResID)
        binding.ivIcon.setImageDrawable(drawable)
    }
}

class TextLabeledFieldViewHolder(private val binding: RvItemTextlabeledFieldBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(field: Field.TextLabeledField) {
        val resID = field.stringResID
        val text = if (resID != null) itemView.context.resources.getString(resID) else ""
        binding.tvLabeledField.text = text
    }
}
