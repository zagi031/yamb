package com.example.yamb.ui.diceThrowingFragment

import android.app.Application
import android.media.SoundPool
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.yamb.R
import com.example.yamb.models.Die
import com.example.yamb.models.DieUI
import com.example.yamb.models.toDieUI
import com.example.yamb.ui.SingleLiveEvent

class DiceThrowingViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        private const val MAX_NUM_OF_THROWS = 3
    }

    private val diceList = listOf(Die(1), Die(2), Die(3), Die(4), Die(5), Die(6))
    var numOfThrows = 0
        private set
    private val _dice = MutableLiveData<List<DieUI>>()
    val dice: LiveData<List<DieUI>> = _dice
    private val _toast = SingleLiveEvent<String>()
    val toast: LiveData<String> = _toast
    private val _numberOfThrows = MutableLiveData<Int>()
    val numberOfThrows: LiveData<Int> = _numberOfThrows

    fun throwDice() {
        if (numOfThrows < 3) {
            numOfThrows++
            playSound()
            this.diceList.forEach { die ->
                if (!die.isHold) {
                    die.throwDie()
                }
            }
            _dice.postValue(this.diceList.map { it.toDieUI() })
            _toast.postValue("${MAX_NUM_OF_THROWS - numOfThrows} throws left")
            _numberOfThrows.postValue(numOfThrows)
        } else _toast.postValue(getApplication<Application>().resources.getString(R.string.alertMessage_3timesThrown))
    }

    fun isDieHold(id: Int) = diceList.first { it.id == id }.isHold

    fun resetNumberOfThrows() {
        this.numOfThrows = 0
        _numberOfThrows.postValue(numOfThrows)
        diceList.forEach { die ->
            die.isHold = false
        }
    }

    fun handleDieClickEvent(dieID: Int) {
        val die = this.diceList.first { it.id == dieID }
        die.isHold = !die.isHold
        _dice.postValue(this.diceList.map { it.toDieUI() })
    }

    fun getDiceIDs() = diceList.map { it.id }

    fun getColorForImageButtonBorder(dieID: Int): Int {
        val die = this.diceList.filter { it.id == dieID }[0]
        return if (die.isHold) R.color.black else R.color.transparent
    }

    private lateinit var soundPool: SoundPool
    private var soundIsLoaded: Boolean = false
    private var throwingDiceSoundID: Int = -1

    init {
        loadSound()
    }

    private fun loadSound() {
        soundPool = SoundPool.Builder().setMaxStreams(1).build()
        soundPool.setOnLoadCompleteListener { _, _, _ -> soundIsLoaded = true }
        throwingDiceSoundID = soundPool.load(getApplication(), R.raw.sound_dice_throwing, 1)
    }

    private fun playSound() {
        if (soundIsLoaded)
            soundPool.play(throwingDiceSoundID, 1f, 1f, 1, 0, 1f)
    }


}