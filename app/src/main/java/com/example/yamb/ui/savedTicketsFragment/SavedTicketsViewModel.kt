package com.example.yamb.ui.savedTicketsFragment

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.yamb.R
import com.example.yamb.models.YambTicket
import com.example.yamb.models.repositories.TicketsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class SavedTicketsViewModel(application: Application, private val repository: TicketsRepository) :
    AndroidViewModel(application) {
    val emptyRecyclerViewMessage = application.resources.getString(R.string.label_no_saved_games)

    fun getSavedTickets(): LiveData<List<YambTicket>> {
        val result = viewModelScope.async(Dispatchers.IO) {
            repository.getSavedTickets()
        }
        // potrebna transformacija, YambTicketUI
        return runBlocking {
            result.await()
        }
    }

    fun deleteSavedTicket(ticket: YambTicket) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteSavedTicket(ticket)
        }
    }

}