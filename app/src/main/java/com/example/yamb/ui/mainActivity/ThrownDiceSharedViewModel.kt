package com.example.yamb.ui.mainActivity

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.yamb.models.DieUI
import com.example.yamb.ui.SingleLiveEvent

class ThrownDiceSharedViewModel : ViewModel() {
    var numberOfThrows: Int? = null
    var thrownDice: List<DieUI>? = null
    private val _isChangedField = SingleLiveEvent<Boolean>()
    val isChangedField: LiveData<Boolean> = _isChangedField

    fun notifyThatFieldIsChanged() {
        _isChangedField.postValue(true)
    }

    fun resetStates() {
        this.numberOfThrows = null
        this.thrownDice = null
    }
}

// reason that all of variables are not live data is that on live data we need some reaction,
// but we use states here just in case when we need it so we don't need to store these states into another viewmodel