package com.example.yamb.ui.savedTicketsFragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.yamb.R
import com.example.yamb.databinding.FragmentSavedTicketsBinding
import com.example.yamb.interfaces.OnSavedTicketClickListener
import com.example.yamb.ui.savedTicketsFragment.adapters.NoneSavedTicketsViewHolder
import com.example.yamb.ui.savedTicketsFragment.adapters.SavedTicketItemDecorator
import com.example.yamb.ui.savedTicketsFragment.adapters.SavedTicketsRecyclerViewAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class SavedTicketsFragment : Fragment(), OnSavedTicketClickListener {
    private lateinit var binding: FragmentSavedTicketsBinding
    private lateinit var savedTicketsAdapter: SavedTicketsRecyclerViewAdapter
    private val viewModel by viewModel<SavedTicketsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSavedTicketsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.progressbar.visibility = View.VISIBLE
        binding.rvSavedTickets.visibility = View.INVISIBLE
        setupRecyclerView()
        viewModel.getSavedTickets().observe(viewLifecycleOwner, {
            savedTicketsAdapter.setTickets(it)
            binding.progressbar.visibility = View.GONE
            binding.rvSavedTickets.visibility = View.VISIBLE
        })
    }

    private fun setupRecyclerView() {
        binding.apply {
            savedTicketsAdapter =
                SavedTicketsRecyclerViewAdapter(
                    viewModel.emptyRecyclerViewMessage,
                    this@SavedTicketsFragment
                )
            rvSavedTickets.apply {
                adapter = savedTicketsAdapter
                layoutManager = LinearLayoutManager(
                    this@SavedTicketsFragment.parentFragment?.context,
                    RecyclerView.VERTICAL,
                    false
                )
                addItemDecoration(SavedTicketItemDecorator(10, 10))
            }
        }
        val touchCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun getSwipeDirs(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ): Int {
                return if (viewHolder is NoneSavedTicketsViewHolder) 0 else super.getSwipeDirs(
                    recyclerView,
                    viewHolder
                )
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                AlertDialog.Builder(activity).setTitle(resources.getString(R.string.label_warning))
                    .setMessage(resources.getString(R.string.message_alertDialogOnDelete))
                    .setPositiveButton(resources.getString(R.string.label_yes)) { _, _ ->
                        viewModel.deleteSavedTicket(savedTicketsAdapter.getTicket(viewHolder.adapterPosition)!!)
                    }.setNegativeButton(resources.getString(R.string.label_cancel)) { dialog, _ ->
                        dialog.cancel()
                    }.setOnCancelListener {
                        savedTicketsAdapter.notifyItemChanged(viewHolder.adapterPosition)
                    }.create().show()
            }
        }
        ItemTouchHelper(touchCallback).attachToRecyclerView(binding.rvSavedTickets)
    }

    override fun onSavedTicketClick(position: Int) {
        val savedTicket = savedTicketsAdapter.getTicket(position)
        if (savedTicket != null) {
            findNavController().navigate(
                SavedTicketsFragmentDirections.actionSavedTicketsFragmentToYambTicketFragment(
                    savedTicket.startTime
                )
            )
        }
    }

}