# Yamb

Yamb is an Android app for playing popular game called [Yahtzee](https://en.wikipedia.org/wiki/Yahtzee). App is completely offline for a single person use. At the end of each game, your finished Yahtzee game ticket will be saved, so you can always see your achieved scores.

To download .apk file, click [here](https://gitlab.com/zagi031/yamb/-/blob/main/yamb.apk).

Technologies used: Android, Kotlin, RecyclerView, Room offline database, Koin, Navigation Component
